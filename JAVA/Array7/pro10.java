import java.io.*;
class Demo10{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows and columns: ");
		int row=Integer.parseInt(br.readLine());
		int col=Integer.parseInt(br.readLine());
		int arr[] []=new int[row][col];
		System.out.println("Enter array Elements: ");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}
		System.out.println("Corner elements are: ");
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				if(i==0 || i==(row-1)){
				       	if(j==(row-1)||j==0 ){
					System.out.print(arr[i][j]+" ");
					}
				}
			}
		}
	}
}



