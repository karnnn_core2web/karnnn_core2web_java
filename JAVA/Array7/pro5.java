import java.io.*;
class Demo5{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows and columns: ");
		int row=Integer.parseInt(br.readLine());
		int col=Integer.parseInt(br.readLine());
		int arr[] []=new int[row][col];
		System.out.println("Enter array Elements: ");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}
		for(int i=0;i<arr.length;i++){
			int sum=0;
			for(int j=0;j<arr.length;j++){
				sum=sum+arr[j][i];
			}
				System.out.println("Sum of column "+(i+1)+" = "+sum);
		}
	}
}


