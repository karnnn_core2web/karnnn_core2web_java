import java.io.*;
class Demo9{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows and columns: ");
		int row=Integer.parseInt(br.readLine());
		int col=Integer.parseInt(br.readLine());
		int arr[] []=new int[row][col];
		System.out.println("Enter array Elements: ");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}
		int sum=0;
		int sum1=0;
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				if(i==j){
					sum1=sum1+arr[i][j];
				}

				if(j==(row-i)-1){
					sum=sum+arr[i][j];
				}
			}
		}
		System.out.println("Product of sum of primary and secondary diagonal elements: "+(sum*sum1));
	}
}



