import java.io.*;
class Demo2{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows and columns: ");
		int row=Integer.parseInt(br.readLine());
		int col=Integer.parseInt(br.readLine());
		int arr[] []=new int[row][col];
		int sum=0;
		System.out.println("Enter array Elements: ");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
				sum=arr[i][j]+sum;
			}
		}
		System.out.println("Sum= "+sum);
	}
}


