import java.io.*;
class Demo7{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of rows: ");
		int rows=Integer.parseInt(br.readLine());
		int n=1;
		int ch=65;
		for(int i=1;i<=rows;i++){
			for(int sp=rows;sp>i;sp--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				if(i%2==1){
					System.out.print(n +"\t");
				}
				else
				{
					System.out.print((char)(ch) +"\t");
				}
			}
			n++;
			ch++;
			System.out.println();
		}
	}
}
