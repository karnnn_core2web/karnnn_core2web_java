import java.io.*;
class Demo9{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of rows: ");
		int rows=Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			int n=97;
			int ch=65;
			for(int sp=rows;sp>i;sp--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				if(i%2==1){
					if(j<i){
						System.out.print((char)(ch) +"\t");
						ch++;
					}
					else{
						System.out.print((char)(ch) +"\t");
						ch--;
					}
				}
				else
				{
					if(j<i){
						System.out.print((char)(n) +"\t");
						n++;
					}
					else{
						System.out.print((char)(n)+"\t");
						n--;
					}
				}
			}
			System.out.println();
		}
	}
}
