import java.io.*;
class Demo4{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of rows: ");
		int rows=Integer.parseInt(br.readLine());
	        int ch=65;
		for(int i=1;i<=rows;i++){
			for(int sp=rows;sp>i;sp--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				System.out.print((char)(ch) +"\t");
			}
			ch++;
			System.out.println();
		}
	}
}
