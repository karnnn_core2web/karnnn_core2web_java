import java.io.*;
class one{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows:" );
		int rows=Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			for(int space=rows;space>i;space--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				System.out.print("1"+"\t");
			}
		System.out.println();
		}
	}
}


