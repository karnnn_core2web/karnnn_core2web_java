import java.io.*;
class Demo10{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of rows: ");
		int rows=Integer.parseInt(br.readLine());
	        int ch=64+rows;
		for(int i=1;i<=rows;i++){
			for(int sp=rows;sp>i;sp--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				if(j<i){
					System.out.print((char)(ch) +"\t");
					ch++;
				}
				else{
					System.out.print((char)(ch) +"\t");
					ch--;
				}
			}
			ch=(64+rows)-i;
			System.out.println();
		}
	}
}
