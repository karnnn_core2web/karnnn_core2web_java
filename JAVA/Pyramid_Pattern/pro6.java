import java.io.*;
class Demo6{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of rows: ");
		int rows=Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			int n=rows;
			for(int sp=rows;sp>i;sp--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				if(j<i){
					System.out.print(n +"\t");
					n--;
				}
				else
				{
					System.out.print(n +"\t");
					n++;
				}
			}
			System.out.println();
		}
	}
}
