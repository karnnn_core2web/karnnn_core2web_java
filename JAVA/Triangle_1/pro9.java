import java.util.*;
class Demo9{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows=sc.nextInt();
		for(int i=1;i<=rows;i++){
			char ch=(char)(64+rows);
			for(int j=1;j<=(rows-i+1);j++){
				System.out.print(ch-- + " ");
			}
			System.out.println();
		}
	}
}

