import java.util.*;
class Demo10{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows=sc.nextInt();
		for(int i=1;i<=rows;i++){
			int ch=64+i;
			for(int j=1;j<=(rows-i+1);j++){
				if(rows%2==0){
					if(ch%2==0){
						System.out.print((char)ch + " ");
					}
					else{
						System.out.print(ch +" ");
					}
		
				}
				else{
					if(ch%2!=0){
						System.out.print((char)ch + " ");
					}
					else{
						System.out.print(ch +" ");
					}
				}
				ch++;

			}
			System.out.println();
		}
	}
}

