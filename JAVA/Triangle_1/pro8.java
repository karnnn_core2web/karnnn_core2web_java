import java.util.*;
class Demo8{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows=sc.nextInt();
		for(int i=1;i<=rows;i++){
			int n=i;
			for(int j=1;j<=(rows-i+1);j++){
				System.out.print(n++ + " ");
			}
			System.out.println();
		}
	}
}

