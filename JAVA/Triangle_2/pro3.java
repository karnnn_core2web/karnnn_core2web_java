import java.io.*;
class Demo3{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number of rows: ");
		int rows=Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			char ch= (char)(64+rows); 
			for(int j=1;j<=i;j++){
				System.out.print(ch-- +" ");
			}
			System.out.println();
		}
	}

}
