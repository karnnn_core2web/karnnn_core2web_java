import java.io.*;
class Demo9{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number of rows: ");
		int rows=Integer.parseInt(br.readLine());
		char ch=97;
		for(int i=1;i<=rows;i++){
			int n=rows+1;
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(n + " ");
				}
				else{
					System.out.print(ch + " ");
					ch++;
				}
				n++;
				
			}
			System.out.println();
		}
	}
}

