import java.io.*;
class Demo6{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		int space=0;
		int col=0;
		int n=0;
		for(int i=1;i<row*2;i++){
			if(i<=row){
				space=row-i;
				col=i*2-1;
				n++;
			}
			else{
				space=i-row;
				col=col-2;
				n--;

			}
			for(int sp=1;sp<=space;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=col;j++){
				System.out.print(n+"\t");
			}
			System.out.println();
		}
	}
}
