import java.io.*;
class Demo4{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		int space=0;
		int col=0;
		for(int i=1;i<row*2;i++){
			int n=1;
			if(i<=row){
				space=row-i;
				col=i*2-1;
			}
			else{
				space=i-row;
				col=col-2;

			}
			for(int sp=1;sp<=space;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=col;j++){
				if(i<=row &&j<i){
					System.out.print(n++ +"\t");
				}
				else if(i>row && j>(row*2)-i){
					System.out.print(n-- +"\t");
				}
				else if(i>row && j<(row*2)-i){
					System.out.print(n++ +"\t");
				}
				else{
					System.out.print(n-- +"\t");
				}

			}
			System.out.println();
		}
	}
}
