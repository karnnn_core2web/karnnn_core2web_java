import java.io.*;
class Demo2{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		int ch=64+row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(j<=row-i){
					System.out.print((char)(ch+32)+" ");
				}
				else{
					System.out.print((char)(ch)+" ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}



