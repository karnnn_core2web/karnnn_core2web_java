import java.io.*;
class Demo6{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int n=row*row;
			for(int j=1;j<=row;j++){
				if(i%2==0){
					if(j==2 ||j==3){
						System.out.print((n-5) +" ");
					}
					else if(j==1){
						System.out.print(n +" ");
					}
					else{
						System.out.print((n-10) +" ");
					}
				}
				else{
					System.out.print(n +" ");
					n--;
				}

			}
			System.out.println();
		}
	}
}



