import java.io.*;
class Demo3{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		int ch=96+row;
		int n=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(row%2==1){
					if(n%2==0){
						System.out.print(n+" ");
					}
					else{
						System.out.print((char)(ch)+" ");
					}
				}
				else{
					if(i%2==1){
						if(j%2==0){
							if(n%2==1){
								System.out.print(n+" ");
							}
						}
						else{
							System.out.print((char)(ch) +" ");
						}
					   }
					else{
						if(j%2==1){
							if(n%2==0){
								System.out.print(n+" ");
							}
						}
						else{
							System.out.print((char)(ch)+" ");
						
						}
					}
				}
				ch++;
				n++;
			}

			System.out.println();
		}
	}
}



