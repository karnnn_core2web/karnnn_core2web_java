import java.io.*;
class Demo2{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number of rows: ");
		int rows=Integer.parseInt(br.readLine());
		int n=rows;
		for(int i=1;i<=rows;i++){
			char ch=(char)(64+rows);
			for(int j=1;j<=rows;j++){
					System.out.print(ch+""+n+ " ");
				        n--;
			}
			n=rows+i;
			System.out.println();
		}
	}
}

