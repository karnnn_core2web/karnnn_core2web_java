import java.io.*;
class Demo10{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number: ");
		long n=Long.parseLong(br.readLine());
		long rev=0;
		while(n!=0){
			long k=n%10;
			rev=rev*10+k;
			n/=10;
		}
		while(rev!=0){
			long p=rev%10;
			rev/=10;
			if(p%2!=0){
			       System.out.print(p*p +", ");
			}
		}
	}
}



