import java.io.*;
class Demo7{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number of rows: ");
		int rows=Integer.parseInt(br.readLine());
		int n=1;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=(rows-i+1);j++){
					System.out.print(2*n + " ");
				        n++;
			}
			System.out.println();
		}
	}
}

