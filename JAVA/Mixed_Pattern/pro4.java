import java.io.*;
class Demo4{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number of rows: ");
		int rows=Integer.parseInt(br.readLine());
		int n=rows;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
					System.out.print(n*j + " ");
			}
			n--;
			System.out.println();
		}
	}
}

