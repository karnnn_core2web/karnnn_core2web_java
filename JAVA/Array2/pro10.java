import java.util.*;
class Demo10{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size=sc.nextInt();
		int sum=0;
		int arr[]=new int[size];
		System.out.println("Enter array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int max=arr[0];
		int k=0;
		for(int i=1;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			        k=i;
			}
		}
		System.out.println("Maximum number in the array is: "+max+" at index "+k);
	}
}


