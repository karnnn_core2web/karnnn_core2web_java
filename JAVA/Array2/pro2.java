
import java.util.*;
class Demo2{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of the array: ");
		int size=sc.nextInt();
		int sum=0;
		int arr[]=new int[size];
		System.out.println("Enter array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Even elements from array are: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%3==0){
				System.out.println(arr[i]);
				sum=sum+arr[i];
			}
		}
		System.out.println("Sum of elements divisible by 3 are: "+sum);
	}
}


