class p3{
    public static void main(String[] args){
        float pi = 3.14f;
        float radius = 1.0f;
        double area = pi*radius*radius;

        System.out.println("Area of circle is "+area );

        int deg = 360;

        System.out.println("Highest Degree of circle is "+ deg);

    }
}