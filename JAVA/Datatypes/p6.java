class p6{
    public static void main(String[] args){

        int day = 8;
        int month = 3;
        int year = 2024;

        System.out.println("Date: " + day);
        System.out.println("Month: " + month);
        System.out.println("Year: " + year);

        int totalSecDay = 24 * 60 * 60;
        System.out.println("Total seconds in a day: " + totalSecDay);

        int totalSecMonth = 30 * totalSecDay;
        System.out.println("Total seconds in a month: " + totalSecMonth);

        int totalSecYear = 365 * totalSecDay;
        System.out.println("Total seconds in a year: " + totalSecYear);

    }
}