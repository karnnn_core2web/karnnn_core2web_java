import java.io.*;
class Demo3{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size: ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		int count=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<size/2;i++){
			if(arr[i]!=arr[size-i-1]){
				count=1;
				break;
			}
		}
		if(count==1){
			System.out.print("The given array is not palindrome.");
		}
		else{
			System.out.print("The given array is palindrome.");
		}
	}
}


