import java.util.*;
class Demo10{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter Size : ");
                int size=sc.nextInt();
                int arr[]=new int[size];
                System.out.println("Enter elements:");
                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                System.out.println("Factorial of each number is :");
                for(int i=0;i<size;i++){
                        int num=arr[i];
                        int fact=1;
                        for(int j=1;j<=arr[i];j++){
                                fact=fact*num;
                                num--;
                        }
                        System.out.println(fact+" ");
                }
        }
}
