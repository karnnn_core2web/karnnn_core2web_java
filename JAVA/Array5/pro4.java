import java.io.*;
class FirstDuplicateIndex {
       	public static void main(String[] args) throws IOException {
       		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.print("Enter the size of the array: ");
        	 int size = Integer.parseInt(br.readLine());
		 int[] arr = new int[size];
		 int count=0;
        	 System.out.println("Enter the elements of the array:");
                 for (int i = 0; i < size; i++) {
            		arr[i] = Integer.parseInt(br.readLine());
        	}
        	for (int i = 0; i < size; i++) {
            		for (int j = i + 1; j < size; j++) {
                		if (arr[i] == arr[j]) {
                    			count++;
					while(count==1){
						System.out.print("First Duplicate element index: "+i);
						break;
					}
				}
			}
        	}
       
    	}
}


