import java.io.*;
class Ascend{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		int count=0;
		System.out.println("Enter elements of the array: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			}
		for(int i=0;i<arr.length-1;i++){
			if(arr[i]<arr[i+1]){
				count++;
			}
			else{
				count--;
			}
		}
		if(count==arr.length-1){
			System.out.println("The given array is in ascending order.");
		}
		else{
			System.out.println("The given array is not in ascending order.");
		}
	}
}





