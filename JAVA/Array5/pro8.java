import java.util.*;
class Demo8{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter Size : ");
                int size=sc.nextInt();
                int arr[]=new int[size];
                System.out.println("Enter elements:");
                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                int min=arr[0];
                for(int i=1;i<size;i++){
                        if(arr[i]<min){
                                min=arr[i];
                        }
                }
		int min2=0;
		if(min==arr[0]){
                	min2=arr[1];
		}
		else{
			min2=arr[0];
		}
                for(int i=0;i<size;i++){
                        if(arr[i]<min2 && arr[i]>min){
                                min2=arr[i];
                        }
                }
                System.out.println("Second minimum element in array is "+min2);
        }
}
