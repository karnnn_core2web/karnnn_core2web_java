import java.io.*;
class OddEven{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		int k=0;
		int n=0;
		System.out.println("Enter elements of the array: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				k=k+arr[i];
			}
			else{
				n=n+arr[i];
			}
		}
		System.out.println("Odd sum: "+n);
		System.out.println("Even sum: "+k);
	}
		
}





