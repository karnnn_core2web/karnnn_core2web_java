import java.io.*;
class Demo5{
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the size of the array: ");
        int size = Integer.parseInt(reader.readLine());
        int[] arr = new int[size];
	int count=0;
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = Integer.parseInt(reader.readLine());
        }
        System.out.println("Count of digits in each element:");
        for (int num : arr) {
		System.out.print(num+": ");
	    while(num!=0){
		    num/=10;
		    count++;
	    }
	    System.out.println(count);
	}
    }
}
