
import java.io.*;
class Demo8{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of rows: ");
		int row=Integer.parseInt(br.readLine());
	
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			int ch=65;
			for(int j=(row-i)*2+1;j>=1;j--){
				if(j>row-i+1){
					System.out.print((char)(ch) +"\t");
					ch++;
				}
				else{
					System.out.print((char)(ch)+"\t");
					ch--;
				}
			}
			System.out.println();
		}
	}
}

