import java.io.*;
class Demo5{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of rows: ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int n=1;
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=(row-i)*2+1;j>=1;j--){
				System.out.print(n++ +"\t");
			}
			System.out.println();
		}
	}
}

