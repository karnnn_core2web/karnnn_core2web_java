class IfDemo{
   public static void main(String[]args){
           float n=85.00f;
	   if(n>=85.00 && n<=100){
               System.out.println("Passed: first class with distinction");
	   }
	   else if(n>=60.00 && n<85){
      
               System.out.println("Passed: first class.");
	   }
	   else if(n<60 && n>=50){
       
               System.out.println("Passed: Second class.");
	   }
	   else{
    
               System.out.println("Passed.");
	   }
   }
}
