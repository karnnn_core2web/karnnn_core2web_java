class IfDemo{
    public static void main(String[]args){
               float num=120f;
	       if(num==0){
		       System.out.println("Number is zero..Please enter another number");
	       }

	       else if(num%6.0==0){

                     System.out.println(num+" is divisible by 6");
	       }
	       else{
                     System.out.println(num+" is not divisible by 6");
	       }
    }
}
