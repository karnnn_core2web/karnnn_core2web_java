class IfDemo{
      public static void main(String[]args){
             int a=3;
	     int b=4;
	     int c=5;
	     if(a==0 || b==0 || c==0){

		     System.out.println("Invalid");
	     }
	     else if((a*a)+(b*b)==(c*c)){
		     System.out.println("Its a Pythagorian triplet");
	    }
	    else{

		     System.out.println("It is not a Pythagorian triplet");
	    }
      
      }
}
