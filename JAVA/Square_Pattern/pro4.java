import java.util.*;
class Demo4{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the no. of rows: ");
		int row=sc.nextInt();
		int n=row;
		int ch=64+row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(j==1){
					System.out.print((char)ch +" ");
					n++;
				}
				else{
					System.out.print(n++  +" ");
				}
				ch++;
				}
				System.out.println ();
		}
	}
}
