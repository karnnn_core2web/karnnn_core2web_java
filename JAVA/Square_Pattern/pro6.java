import java.util.*;
class Demo6{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the no. of rows: ");
		int row=sc.nextInt();
		int n=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(n%2==1){
					System.out.print(n*n  +" ");
				}
				else{
					System.out.print(n  +" ");
				}
				n++;
			}
			System.out.println ();
		}
	}
}
