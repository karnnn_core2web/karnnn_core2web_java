import java.util.*;
class Demo7{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the no. of rows: ");
		int row=sc.nextInt();
		int n=row;
		for(int i=1;i<=row;i++){
    			int ch=64+i;
			for(int j=1;j<=row;j++){
				if(n%2==0){
					System.out.print(n  +" ");
				}
				else{
					System.out.print((char)(ch)  +" ");
				}
				n++;
			}
			System.out.println ();
		}
	}
}
