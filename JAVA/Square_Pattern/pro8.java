import java.util.*;
class Demo8{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the no. of rows: ");
		int row=sc.nextInt();
		int n=row;
		for(int i=1;i<=row;i++){
   	 	int ch=64+row;
		for(int j=1;j<=row;j++){
    			if(i%2==1){
				if(n%2==1){
					System.out.print("#"  +" ");
				}
				else{
					System.out.print((char)(ch)  +" ");
					ch--;
				}
			}
			else{
			   	if(n%2==1){
   	    	 			System.out.print("#" +" ");
    				}
    				else{
        				System.out.print((char)(ch)+ " ");
        				ch--;
    				}
			}
			n++;
			}
			System.out.println ();
		}
	}
}
