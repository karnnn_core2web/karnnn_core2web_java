import java.util.*;
class Demo5{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of of rows: ");  
		int rows=sc.nextInt();
		for(int i=1;i<=rows;i++){
			for(int space=1;space<=rows-i;space++){
				System.out.print("\t");
				
			}
			for(int j=1;j<=i;j++){
				System.out.print((i*j)+"\t");
				
			}
			System.out.println();
		}
	}
}

