import java.io.*;
class Demo3{
       	public static void main(String[] args)throws IOException {
       	 	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	 	System.out.print("Enter number of rows: ");
		int row=Integer.parseInt(br.readLine());
       		for(int i=1;i<=row;i++){
              		 int ch=64+(row-i+1);
         	for(int space=1;space<=row-i;space++){
                	System.out.print("\t");
            	} 
         	for(int j=1;j<=i;j++){
             
                	System.out.print((char)(ch++) +"\t");
                
            	}
                System.out.println();
		}
	}
}
