import java.util.*;
class Demo7{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of of rows: ");  
		int rows=sc.nextInt();
		for(int i=1;i<=rows;i++){
			for(int space=1;space<i;space++){
				System.out.print("\t");
				
			}
			int n=1;
			for(int j=rows;j>=i;j--){
				System.out.print(n+"\t");
				n++;
				
			}
			System.out.println();
		}
	}
}

