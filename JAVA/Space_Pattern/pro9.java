import java.util.*;
class Demo9{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of of rows: ");  
		int rows=sc.nextInt();
		for(int i=1;i<=rows;i++){
			int ch=64+rows;
			for(int space=1;space<i;space++){
				System.out.print("\t");
				
			}
			for(int j=rows;j>=i;j--){
				System.out.print((char)ch-- +"\t");
				
			}
			System.out.println();
		}
	}
}

