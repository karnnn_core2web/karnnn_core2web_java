import java.util.*;
class Demo10{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of of rows: ");  
		int rows=sc.nextInt();
	
		for(int i=1;i<=rows;i++){
			for(int space=1;space<i;space++){
				System.out.print("\t");
				
			}
			int ch=64+i;
			for(int j=1;j<=(rows-i+1);j++){
				if(rows%2==0){
					if((j-i+1)%2==0){
						System.out.print((char)ch +"\t");
					}
					
					else{
						System.out.print(ch+"\t");
					}
				}else{
				
					if((j-i+1)%2!=0){
						System.out.print((char)ch +"\t");
					}else{
						System.out.print(ch+"\t");
			
					}
				}
			
				ch++;
				
			}
			System.out.println();
		}
	}
}

