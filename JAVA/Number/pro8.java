
import java.io.*;
class Demo8{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number: ");
		int n=Integer.parseInt(br.readLine());
		int rev=0;
		while(n!=0){
			int rem=n%10;
			rev=rev*10+rem;
			n/=10;
		}
	        System.out.print("Reversed number is "+rev);
	}
}

