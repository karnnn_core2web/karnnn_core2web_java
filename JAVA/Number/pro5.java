import java.io.*;
class Demo5{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number: ");
		int n=Integer.parseInt(br.readLine());
		int count=0;
		for(int i=1;i<=n;i++){
			if(n%i==0){
				count++;
			}
		}
		if(count>2){
			System.out.println("Composite number");
		}
		else{
			System.out.println("Not a Composite number");
		}
			
	}
}

