
import java.io.*;
class Demo1{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number: ");
		int n=Integer.parseInt(br.readLine());
		System.out.print("Factors of "+n+" are ");
		for(int i=1;i<=n;i++){
			if(n%i==0){
				System.out.print(i+",");
			}
		}
			
	}
}

