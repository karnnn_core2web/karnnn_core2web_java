
import java.io.*;
class Demo7{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number: ");
		int n=Integer.parseInt(br.readLine());
		int mul=1;
		for(int i=1;i<=n;i++){
			mul=mul*i;
		}
	        System.out.print("Factorial of "+n+" is "+mul);
	}
}

