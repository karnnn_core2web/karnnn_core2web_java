import java.io.*;
class Demo10{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    		System.out.println("Enter number of rows: ");
		int rows=Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			char ch=(char)(64+(rows-i+1));
		        char c=(char)(96+(rows-i+1));
			for(int j=1;j<=(rows-i+1);j++){
				if(rows%2==0){
					if(i%2==1){
						System.out.print(ch-- + " ");
					}
					else{
						System.out.print(c-- +" ");
					}
				}
				else{
					if(i%2!=1){
						System.out.print(ch-- + " ");
					}
					else{
						System.out.print(c-- +" ");
					}
				}

			}
			System.out.println();
		}
	}
}

