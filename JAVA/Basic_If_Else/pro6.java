class IfDemo{
      public static void main(String[]args){
                int n=15;
		if(n==0){
                      System.out.println("Number entered is zero");
		}
		else if(n%3==0 && n%7==0){
                      System.out.println(n+" is divisible by both");
		}
		else if(n%3==0){
                     System.out.println(n+" is divisible by 3");

	       }
	       else if(n%7==0){
                     System.out.println(n+" is divisible by 7");
	       }
               else{
	           System.out.println(n+ " is neither divisible by 3 nor by 7");
	       }

      }
}
