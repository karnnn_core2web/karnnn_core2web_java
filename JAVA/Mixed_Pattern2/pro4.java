
import java.util.*;
class Demo4{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter no. of rows: ");
		int row=sc.nextInt();
		int ch=97;
		int c=65;
		for(int i=1;i<=row;i++){
			for(int space=1;space<i;space++){
				System.out.print("\t");
		}
			for(int j=row;j>=i;j--){
				if(row%2==0){
					System.out.print((char)(ch) +"\t");
					ch++;
				}
				else{
					System.out.print((char)(c) +"\t");
					c++;
				}
			}
                        c=c-(row-i);
			ch=ch-(row-i);
			System.out.println();
		}
	}
}



