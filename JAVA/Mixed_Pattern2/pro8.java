import java.io.*;
class Demo8{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		int n=row;
		for(int i=1;i<=row;i++){
			for(int space=row;space>i;space--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				System.out.print(n++ +"\t");
			}
			n=row-i;
			System.out.println();
		}
	}
}

