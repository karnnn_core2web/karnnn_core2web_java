
import java.util.*;
class Demo3{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter no. of rows: ");
		int row=sc.nextInt();
		int n=1;
		for(int i=1;i<=row;i++){
			for(int space=1;space<=row-i;space++){
				System.out.print("\t");
			}
			for(int j=1;j<=i;j++){
				System.out.print(n +"\t");
				n+=row;
			}
			System.out.println();
		}
	}
}

