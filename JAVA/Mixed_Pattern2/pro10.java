import java.io.*;
class Demo10{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		int n=1;
		for(int i=1;i<=row;i++){
 			for(int space=row;space>i;space--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				if(j<=i){
					System.out.print(n++ +"\t");
				}
				else{
					System.out.print(n-- +"\t");
				}
				
				
			} System.out.println();
		}
	}
}

