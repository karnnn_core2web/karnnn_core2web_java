
import java.util.*;
class Demo2{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter no. of rows: ");
		int row=sc.nextInt();
		int n=1;
		for(int i=1;i<=row;i++){
			for(int space=1;space<=i-1;space++){
				System.out.print("\t");
			}
			for(int j=row;j>=i;j--){
				System.out.print(n +"\t");
				n++;
			}
			n-=1;
			System.out.println();
		}
	}
}




