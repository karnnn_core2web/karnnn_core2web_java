import java.util.*;
class Demo1{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter no. of rows: ");
		int row=sc.nextInt();
		int n=1;
		for(int i=1;i<=row;i++){
			for(int space=1;space<=row-i;space++){
				System.out.print("\t");
			}
			for(int j=1;j<=i;j++){
				if(i<4){
					System.out.print(n +"\t");
					n+=2;
				}
				else{
					if(j<=2){
						System.out.print(n+"\t");
						n+=4;
					}
					else{
						n-=2;
						System.out.print(n+"\t");
						n+=6;
					}
				}
			}
			System.out.println();
		}
	}
}

