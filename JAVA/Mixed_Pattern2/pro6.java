
import java.io.*;
class Demo6{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int n=1;
			for(int space=row;space>i;space--){
				System.out.print("\t");
			}
			for(int j=1;j<=(i*2-1);j++){
				System.out.print(n++ +"\t");
			}
			System.out.println();
		}
	}
}

