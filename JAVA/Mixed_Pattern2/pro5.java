import java.io.*;
class Demo5{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows: ");
		int row=Integer.parseInt(br.readLine());
		int n=3;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==0){
					System.out.print(n +"\t");
				}
				else{
					System.out.print((n*n)+"\t");
				}
			n++;
			}
		System.out.println();
		}
	}
}	
