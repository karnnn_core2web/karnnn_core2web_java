
class WhileDemo{
	public static void main(String[]args){
		int day=7;
		while(day>=0){
			if(day!=0){
				System.out.println(day +" days remaining");
			}
			else{
				System.out.println("0 days Assignment overdue");
			}
			day--;
		}
	}
}
