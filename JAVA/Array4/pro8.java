import java.io.*;
import java.util.*;
class Demo8{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No.of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		int count=0;
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)(br.read());
			br.skip(1);
		}
		Arrays.sort(arr);
		System.out.println("Enter a character to check: ");
		char ch=(char)(br.read());
		for(int i=0;i<arr.length;i++){
			if(arr[i]==ch){
 				count++;
			}
		}
		System.out.println(ch+" occurs "+count+" times in the given array.");
	}
}


