import java.io.*;
import java.util.*;
class Demo4{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		int count=0;
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.print(" ");
		Arrays.sort(arr);
		for(int n:arr){
			for(int i=0;i<arr.length;i++){
				if(n==arr[i]){
					count++;
				}
			}
			if(count>=2){
				System.out.println("The number "+n+" occurred "+count+" times.");
			}
		}
	
	}
}


