import java.io.*;
class Demo7{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No.of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)(br.read());
			br.skip(1);
		}
		System.out.println("Conversion to UpperCase: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]>='a' && arr[i]<='z'){
 				System.out.println((char)(arr[i]-32));
			}
			else{
				System.out.println((char)(arr[i]));
			}

		}
	}
}


