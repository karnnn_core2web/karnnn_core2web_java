import java.io.*;
class Demo9{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No.of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)(br.read());
			br.skip(1);
		}
		System.out.println("After Conversion: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]>='a' && arr[i]<='z'){
 				System.out.println((char)(arr[i]));
			}
			else{
				System.out.println("#");
			}

		}
	}
}


