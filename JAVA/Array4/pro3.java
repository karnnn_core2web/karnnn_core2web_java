import java.io.*;
class Demo3{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		int max=arr[0];
		int max2=0;
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]>max){
				max2=max;
				max=arr[i];
			}
			if(max2<arr[i] && max!=arr[i]){
				max2=arr[i];
			}
		}
		System.out.println("Second largest element: "+max2);
	}
}


