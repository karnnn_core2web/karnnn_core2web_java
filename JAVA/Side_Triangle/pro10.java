import java.util.*;
class Demo10{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the no. of rows: ");
		int row=sc.nextInt();
		int col=0;
		int ch=0;
		for(int i=1;i<row*2;i++){
		    if(i<=row){
		        col=row-i;
		    }
		    else{
		        col=i-row;
		    }
		    for(int sp=1;sp<=col;sp++){
			    System.out.print("\t");
		    }
		    if(i<=row){
			    col=i;
			    ch=(65+row)-i;
		    }
		    else{
			    col=row*2-i;
			    ch=61+i;

		    }
		    for(int j=1;j<=col;j++){
		        System.out.print((char)(ch) +"\t");
			ch++;
		    }
		    System.out.println();
		}
	}
}
