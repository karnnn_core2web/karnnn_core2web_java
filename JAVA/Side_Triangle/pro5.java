import java.util.*;
class Demo5{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the no. of rows: ");
		int row=sc.nextInt();
		int col=0;
		int ch=65+row;
		for(int i=1;i<row*2;i++){
		    
		    if(i<=row){
		        col=i;
		        ch--;
		    }
		    else{
		        col--;
		        ch++;
		    }
		    for(int j=1;j<=col;j++){
		        System.out.print((char)(ch) +" ");
		    }
		    System.out.println();
		}
	}
}
