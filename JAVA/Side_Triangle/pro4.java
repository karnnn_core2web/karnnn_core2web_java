import java.util.*;
class Demo4{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the no. of rows: ");
		int row=sc.nextInt();
		int col=0;
		int n=0;
		for(int i=1;i<row*2;i++){
		    
		    if(i<=row){
		        col=i;
		        n=row-i+1;
		    }
		    else{
		        col--;
		        n++;
		    }
		    for(int j=1;j<=col;j++){
		        System.out.print(n +" ");
		    }
		    System.out.println();
		}
	}
}
