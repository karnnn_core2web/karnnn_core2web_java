import java.util.*;
class StrongNo{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.println("Enter number:");
                int num=sc.nextInt();
                int temp=num;
                int sum=0;
                while(num>0){
                        int digit=num%10;
                        int mult=1;
                        while(digit>=1){
                                mult=mult*digit;
                                digit--;
                        }num/=10;
                        sum=sum+mult;
                }
                if(sum==temp){
                        System.out.println(temp+" is a strong number");
                }else{
                        System.out.println(temp+" is not a strong number");
                }
        }
}



