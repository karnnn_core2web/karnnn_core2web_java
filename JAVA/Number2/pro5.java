import java.util.*;
class AutomorphicNo{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.println("Enter number:");
                int num=sc.nextInt();
                int temp=num;
                int sqr=num*num;
                int digit=0;
                while(num>0){
                        num/=10;
                        digit++;
                }
                int var1=0;
                while(digit>0){
                        int rem=sqr%10;
                        var1=rem+var1*10;
                        sqr/=10;
                        digit--;
                }
                int rev=0;
                while(var1>0){
                        int rem=var1%10;
                        var1/=10;
                        rev=rem+rev*10;
                }
                if(rev==temp){
                        System.out.println(temp+" is Automorphic number");
                }else{
                        System.out.println(temp+" is not an Automorphic number");
                }
        }
}
