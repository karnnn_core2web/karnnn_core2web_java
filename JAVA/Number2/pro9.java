import java.io.*;
class HarshadNo{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a number: ");
		int n=Integer.parseInt(br.readLine());
		int sum=0;
		int temp=n;
		while(n!=0){
			int k=n%10;
			sum=sum+k;
			n=n/10;
		}
		if(temp%sum==0){
			System.out.println(temp+" is a Harshad Number.");
		}
		else{
			System.out.println(temp+" is not a Harshad Number.");
		}
	}
}

