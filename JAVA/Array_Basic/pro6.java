import java.io.*;
class ArrayDemo6{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size: ");
		int size=Integer.parseInt(br.readLine());
		char []arr=new char[size];
		System.out.println("Enter elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)br.read();
			br.skip(1);
		}
		System.out.println("Array elements are: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+","+" ");
		}
	}
}


