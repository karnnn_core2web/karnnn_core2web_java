import java.io.*;
class ArrayDemo9{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size: ");
		int size=Integer.parseInt(br.readLine());
		int []arr=new int[size];
		System.out.println("Enter elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			if(i%2!=0)
			System.out.println(arr[i]+" is an odd indexed element.");
		}
	}
}

