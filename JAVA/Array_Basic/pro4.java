import java.io.*;
class ArrayDemo4{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size: ");
		int size=Integer.parseInt(br.readLine());
		int []arr=new int[size];
		int sum=0;
		System.out.println("Enter elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Odd elements from the array are:");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2!=0){
				sum=sum+arr[i];
			}
		}
		System.out.println("Sum of odd elements is "+sum);
	}
}

