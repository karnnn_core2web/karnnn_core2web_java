import java.io.*;
class ArrayDemo2{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size: ");
		int size=Integer.parseInt(br.readLine());
		int []arr=new int[size];
		System.out.println("Enter elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+","+" ");
		}
	}
}

