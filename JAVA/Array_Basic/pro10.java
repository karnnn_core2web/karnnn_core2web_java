import java.io.*;
class ArrayDemo10{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of tasks to be written in to-do list: ");
		int size=Integer.parseInt(br.readLine());
		String []arr=new String[size];
		int n=1;
		System.out.println("Enter Tasks: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=br.readLine();
		}
		System.out.println("Today's Tasks to be completed are: ");
		for(int i=0;i<arr.length;i++){
			System.out.println(n+". "+arr[i]);
			n++;
		}
	}
}

