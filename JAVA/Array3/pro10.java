import java.io.*;
class Demo10{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		int count=0;
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Prime numbers product: ");
		int k=1;
		for(int n: arr){
				for(int i=1;i<=n;i++){
					if(n%i==0){
						count++;
					}
				}
			
				if(count==2){
					k=k*n;
					System.out.println(k);
				}
			}
	}
}


