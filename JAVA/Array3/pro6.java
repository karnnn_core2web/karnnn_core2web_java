
import java.io.*;
class Demo6{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		char arr[]=new char[size];
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)(br.read());
			br.skip(1);
		}
		System.out.println("Consonants from the array are: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='A'||arr[i]=='a'||arr[i]=='E'||arr[i]=='e'||arr[i]=='I'||arr[i]=='i'||arr[i]=='O'||arr[i]=='o'||arr[i]=='U'||arr[i]=='u'){
				continue;
			}
			else{
				System.out.println(arr[i] +" ");
			}
		}
	}
}


