import java.io.*;
class Demo3{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.print("Specific number: ");
		int n=Integer.parseInt(br.readLine());
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(n!=arr[i]){
				System.out.println("Number not present in array.");
				break;
			}
			else if(arr[i]==n){
				count++;
			}
		}
		System.out.println("Number "+n+" occurred "+ count+" times in an array.");
		
	}
}


