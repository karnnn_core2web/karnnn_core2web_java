import java.io.*;
class Demo4{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("After coversion:");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				arr[i]=0;
			}
			else{
				arr[i]=1;
			}
			System.out.println(arr[i] +" ");
		}
		
		
	}
}


