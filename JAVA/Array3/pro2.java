import java.io.*;
class Demo2{
	public static void main(String[]args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No. of elements to be entered in array: ");
	        int size=Integer.parseInt(br.readLine());	
		int arr[]=new int[size];
		int count=0;
		System.out.println("Enter the array elements: ");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.print("Specific number: ");
		int n=Integer.parseInt(br.readLine());
		for(int i=0;i<arr.length;i++){
			if(arr[i]==n){
				System.out.println(arr[i]+" is at index "+i);
				break;
			}

		}
	}
}


