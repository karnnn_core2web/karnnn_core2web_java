import java.util.*;
class Demo10{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter Size : ");
                int size=sc.nextInt();
                int arr[]=new int[size];
                System.out.println("Enter elements:");
                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                int max=arr[0];
                for(int i=1;i<size;i++){
                        if(arr[i]>max){
                                max=arr[i];
                        }
                }
		int max1=0;
		if(max==arr[0]){
                	 max1=arr[1];
		}
		else{
			max1=arr[0];
		}
                for(int i=1;i<size;i++){
                        if(arr[i]>=max1 && arr[i]<max){
                                max1=arr[i];
                        }
                }
		int max2=0;
		if(max1==arr[1]){
                 	max2=arr[2];
		}
		else{
			max2=arr[0];
		}
                for(int i=1;i<size;i++){
                        if(arr[i]>=max2 && arr[i]<max1){
                                max2=arr[i];
                        }
                }
                System.out.println("Third largest element in an array is "+max2);
        }
}
