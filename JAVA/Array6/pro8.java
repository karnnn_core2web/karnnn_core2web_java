import java.util.*;
class Demo8{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter Size : ");
                int size=sc.nextInt();
                char arr[]=new char[size];
                System.out.println("Enter elements:");
                for(int i=0;i<size;i++){
                        arr[i]=sc.next().charAt(0);
                }
                System.out.println("Array alternate elements before reverse");
                for(int i=0;i<size;i++){
                        if(i%2==0){
                                 System.out.println(arr[i]);
                        }
                }
                for(int i=0;i<size/2;i++){
                        char temp=arr[i];
                        arr[i]=arr[size-1-i];
                        arr[size-1-i]=temp;
                }
                System.out.println("Reverse Array: ");
                for(int i=0;i<size;i++){
                        System.out.println(arr[i]);
                }

        }
}
