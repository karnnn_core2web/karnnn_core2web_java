import java.util.*;
class Demo6{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter Size : ");
                int size=sc.nextInt();
                int arr[]=new int[size];
                System.out.println("Enter elements:");
                for(int i=0;i<size;i++){
                        arr[i]=sc.nextInt();
                }
                System.out.println("Enter number:");
                int num=sc.nextInt();
                for(int i=0;i<size;i++){
                        if(arr[i]%num==0){
                                System.out.println("An element multiple of "+num+" found at index : "+i);
                        }
                }
        }
}
