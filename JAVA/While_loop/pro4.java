
class LoopDemo{
	public static void main(String[]args){
		int start=1;
		int end=6;
		char ch='A';
		while(start<=end){
			if(start%2==0){
				System.out.print(start+" ");
			}
			else{
				System.out.print(ch+" ");
			}
			ch++;
			start++;
		}
		System.out.println();
	}
}

