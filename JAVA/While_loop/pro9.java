class WhileDemo{
	public static void main(String[]args){
		int n=214367689;
		int c=0;
		int m=0;
		while(n!=0){
			int k=n%10;
			n=n/10;
			if(k%2==0){
				c++;
			}
			else{
				m++;   
			}
		}
		System.out.println("Odd count: "+m);
		System.out.println("Even count: "+c);
	}
}
