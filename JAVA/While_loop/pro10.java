class LoopDemo{
	public static void main(String[]args){
		long n=9307922405l;
		long sum=0;
		while(n!=0){
			long k=n%10;
			n=n/10;
			sum=sum+k;
		}
		System.out.println("Sum of digits in 9307922405 is "+sum);

	}

}
